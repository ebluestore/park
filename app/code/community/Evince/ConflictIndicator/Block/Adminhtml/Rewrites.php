<?php
 
class Evince_ConflictIndicator_Block_Adminhtml_Rewrites extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'evince_conflictIndicator';
        $this->_controller = 'adminhtml_rewrites';
        $this->_headerText = Mage::helper('evince_conflictIndicator')->__('Evince Conflict Indicator');
        parent::__construct();
        $this->removeButton('add');
    }

}