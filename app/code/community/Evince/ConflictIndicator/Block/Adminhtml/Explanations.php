<?php
 
class Evince_ConflictIndicator_Block_Adminhtml_Explanations extends Mage_Adminhtml_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('evince/conflictIndicator/explanations.phtml');
    }
    
    public function getConflictColor()
    {
        return Evince_ConflictIndicator_Model_Rewrites::CONFLICT_COLOR;
    }
    
    public function getNoConflictColor()
    {
        return Evince_ConflictIndicator_Model_Rewrites::NO_CONFLICT_COLOR;
    }

    public function getConflictResolvedColor()
    {
        return Evince_ConflictIndicator_Model_Rewrites::RESOLVED_CONFLICT_COLOR;
    }    
}