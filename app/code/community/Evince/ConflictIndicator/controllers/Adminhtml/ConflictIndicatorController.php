<?php
 
class Evince_ConflictIndicator_Adminhtml_ConflictIndicatorController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }    
    
        $this->loadLayout();
        $this->_setActiveMenu('system/modules_conflict_detector');  
        $this->_addContent(
            $this->getLayout()->createBlock('evince_conflictIndicator/adminhtml_rewrites', 'modules_rewrites')
        );
        $this->_addContent(
            $this->getLayout()->createBlock('evince_conflictIndicator/adminhtml_explanations', 'explanations')
        );    
        $this->renderLayout();
    }
    
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('evince_conflictIndicator/adminhtml_rewrites_grid')->toHtml());
    }
}