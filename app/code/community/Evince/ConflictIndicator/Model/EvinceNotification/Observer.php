<?php
 
class Evince_ConflictIndicator_Model_EvinceNotification_Observer
{
    public function preDispatch(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('admin/session')->isLoggedIn()) {
            $feedModel  = Mage::getModel('evince_conflictIndicator/evinceNotification_feed');
            $feedModel->checkUpdate();
        }

    }
}