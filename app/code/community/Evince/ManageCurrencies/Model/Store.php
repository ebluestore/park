<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   Evince
 * @package    Evince_ManageCurrencies
 * @copyright  Copyright (c) 2012 - 2015 Evince Development (http://www.evincedev.com/)
 * @contacts   support@evincedev.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Evince_ManageCurrencies_Model_Store extends Mage_Core_Model_Store
{

    /**
     * Round price
     *
     * @param mixed $price
     * @return double
     */
    public function roundPrice($price)
    {
        // fixed double rounding for stores, which use non base display currency and product prices include taxes
        
        if (Mage::app()->getStore()->getDoNotRoundET()) {
            return $price;
        }

        $options = Mage::helper('managecurrencies')->getOptions(array());
        return round($price, isset($options["precision"])?$options["precision"]:2);
    }
}
