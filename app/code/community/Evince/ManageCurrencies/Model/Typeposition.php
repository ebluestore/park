<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   Evince
 * @package    Evince_ManageCurrencies
 * @copyright  Copyright (c) 2012 - 2015 Evince Development (http://www.evincedev.com/)
 * @contacts   support@evincedev.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Evince_ManageCurrencies_Model_Typeposition extends Varien_Object
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 8, 'label'=>Mage::helper('managecurrencies')->__('Default')),
            array('value' => 16, 'label'=>Mage::helper('managecurrencies')->__('Right')),
            array('value' => 32, 'label'=>Mage::helper('managecurrencies')->__('Left')),
        );
    }
}