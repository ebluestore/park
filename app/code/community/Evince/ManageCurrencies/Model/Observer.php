<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   Evince
 * @package    Evince_ManageCurrencies
 * @copyright  Copyright (c) 2012 - 2015 Evince Development (http://www.evincedev.com/)
 * @contacts   support@evincedev.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Evince_ManageCurrencies_Model_Observer
{

    public function fixCurrencySwitchUrl(Varien_Event_Observer $observer)
    {
        $isFixEnabled = (int)Mage::getConfig()->getNode('default/managecurrencies/additional/fix_currency_switch_url');
        if ($isFixEnabled) {
            //helper rewrite
            Mage::getConfig()->setNode('global/helpers/directory/rewrite/url', 'Evince_ManageCurrencies_Helper_Url');

            //controller rewrite
            Mage::getConfig()->setNode('global/rewrite/managecurrencies_switch_currency/from',
                '#^/directory/currency#');
            Mage::getConfig()->setNode('global/rewrite/managecurrencies_switch_currency/to',
                '/managecurrencies/currency');
        }
    }

    public function rewriteClasses(Varien_Event_Observer $observer)
    {
        $isRewriteEnabled = (int)Mage::getConfig()->getNode('default/managecurrencies/additional/rewrite_classes');
        if ($isRewriteEnabled) {
            /** in CE version 1.8.1.0 tax functions declarations changed */
            if (version_compare(Mage::getVersion(), '1.8.1', '>')) {
                //Helper rewrite
                Mage::getConfig()->setNode('global/helpers/tax/rewrite/data', 'Evince_ManageCurrencies_Helper_Tax1810');
            } else {
                //Helper rewrite
                Mage::getConfig()->setNode('global/helpers/tax/rewrite/data', 'Evince_ManageCurrencies_Helper_Tax');
            }
        }
    }
}