<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   Evince
 * @package    Evince_ManageCurrencies
 * @copyright  Copyright (c) 2012 - 2015 Evince Development (http://www.evincedev.com/)
 * @contacts   support@evincedev.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Evince_ManageCurrencies_Model_Locale extends Mage_Core_Model_Locale
{
    public function currency($ccode)
    {
        $admcurrency = parent::currency($ccode);
        $options = Mage::helper('managecurrencies')->getOptions(array(), true, $ccode);
        $admcurrency->setFormat($options, $ccode);

        return $admcurrency;
    }


    public function getJsPriceFormat()
    {
        // For JavaScript prices
        $parentFormat = parent::getJsPriceFormat();
        $options = Mage::helper('managecurrencies')->getOptions(array());
        if (isset($options["precision"])) {
            $parentFormat["requiredPrecision"] = $options["precision"];
            $parentFormat["precision"] = $options["precision"];
        }

        return $parentFormat;
    }
}