<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   Evince
 * @package    Evince_ManageCurrencies
 * @copyright  Copyright (c) 2012 - 2015 Evince Development (http://www.evincedev.com/)
 * @contacts   support@evincedev.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Evince_ManageCurrencies_Model_Typesymboluse extends Varien_Object
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('managecurrencies')->__('Do not use')),
            array('value' => 2, 'label'=>Mage::helper('managecurrencies')->__('Use symbol')),
            array('value' => 3, 'label'=>Mage::helper('managecurrencies')->__('Use short name')),
            array('value' => 4, 'label'=>Mage::helper('managecurrencies')->__('Use name')),
        );
    }
}
