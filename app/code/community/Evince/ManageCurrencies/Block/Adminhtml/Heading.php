<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   Evince
 * @package    Evince_ManageCurrencies
 * @copyright  Copyright (c) 2012 - 2015 Evince Development (http://www.evincedev.com/)
 * @contacts   support@evincedev.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

//if (!class_exists('Mage_Adminhtml_Block_System_Config_Form_Field_Heading')) {
// https://bugs.php.net/bug.php?id=52339
if (version_compare(Mage::getVersion(), '1.4.1', '<')) {
    class Evince_ManageCurrencies_Block_Adminhtml_Heading
        extends Mage_Adminhtml_Block_Abstract
            implements Varien_Data_Form_Element_Renderer_Interface
    {

        public function render(Varien_Data_Form_Element_Abstract $element)
        {
            return sprintf(
                '<tr class="system-fieldset-sub-head" id="row_%s"><td colspan="5"><h4 id="%s">%s</h4></td></tr>',
                $element->getHtmlId(), $element->getHtmlId(), $element->getLabel()
            );
        }
    }
} else {
    class Evince_ManageCurrencies_Block_Adminhtml_Heading extends Mage_Adminhtml_Block_System_Config_Form_Field_Heading
    {
    }
}