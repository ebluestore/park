<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   Evince
 * @package    Evince_ManageCurrencies
 * @copyright  Copyright (c) 2012 - 2015 Evince Development (http://www.evincedev.com/)
 * @contacts   support@evincedev.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Evince_ManageCurrencies_Block_Adminhtml_Formprice  extends Mage_Adminhtml_Block_Catalog_Product_Helper_Form_Price
{
    public function getEscapedValue($index=null)
    {
        $options = Mage::helper('managecurrencies')->getOptions(array());
        $value = $this->getValue();

        if (!is_numeric($value)) {
            return null;
        }

        if (isset($options["input_admin"]) && isset($options['precision'])) {
            return number_format($value, $options['precision'], null, '');
        }

        return parent::getEscapedValue($index);
    }
}