<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   Evince
 * @package    Evince_ManageCurrencies
 * @copyright  Copyright (c) 2012 - 2015 Evince Development (http://www.evincedev.com/)
 * @contacts   support@evincedev.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Evince_ManageCurrencies_Block_Js  extends Mage_Core_Block_Template
{
    public function getJsonConfig()
    {
        if (method_exists(Mage::helper('core'), 'jsonEncode')) {
            return Mage::helper('core')->jsonEncode(
                Mage::helper('managecurrencies')->getOptions(
                    array(),
                    false,
                    Mage::app()->getStore()->getCurrentCurrencyCode()
                )
            );
        } else {
            return Zend_Json::encode(
                Mage::helper('managecurrencies')->getOptions(
                    array(),
                    false,
                    Mage::app()->getStore()->getCurrentCurrencyCode()
                )
            );
        }
    }
}