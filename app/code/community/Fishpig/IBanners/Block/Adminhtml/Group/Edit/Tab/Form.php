<?php
/**
 * @category    Fishpig
 * @package     Fishpig_iBanners
 * @license     http://fishpig.co.uk/license.txt
 * @author      Ben Tideswell <help@fishpig.co.uk>
 */

class Fishpig_iBanners_Block_Adminhtml_Group_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('group_');
        $form->setFieldNameSuffix('group');
        
		$this->setForm($form);
		
		$fieldset = $form->addFieldset('group_general', array('legend'=> $this->__('General Information')));

		$fieldset->addField('title', 'text', array(
			'name' 		=> 'title',
			'label' 	=> $this->__('Title'),
			'title' 	=> $this->__('Title'),
			'required'	=> true,
			'class'		=> 'required-entry',
		));
		
		$fieldset->addField('code', 'text', array(
			'name' 		=> 'code',
			'label' 	=> $this->__('Code'),
			'title' 	=> $this->__('Code'),
			'note'		=> $this->__('This is a unique identifier that is used to inject the banner group via XML'),
			'required'	=> true,
			'class'		=> 'required-entry validate-code',
		));

		$fieldset->addField('randomise_banners', 'select', array(
			'name' => 'randomise_banners',
			'comment' => $this->__('This is for groups with more than 1 banner in'),
			'title' => $this->__('Randomise Banner Position'),
			'label' => $this->__('Randomise Banner Position'),
			'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
		));
		
		$fieldset->addField('is_enabled', 'select', array(
			'name' => 'is_enabled',
			'title' => $this->__('Enabled'),
			'label' => $this->__('Enabled'),
			'required' => true,
			'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
		));

		$fieldset->addField('store_id', 'select', array(
			'name'		=> 'store_id',
			'label'		=> $this->__('Store'),
			'title'		=> $this->__('Store'),
			'required'	=> true,
			'class'		=> 'required-entry',
			'values'	=> $this->_getStores()
		));

		$fieldset = $form->addFieldset('group_settings', array('legend'=> $this->__('Slider Settings')));
		
		$fieldset->addField('number_items', 'text', array(
			'name' => 'number_items',
			'title' => $this->__('Number Items'),
			'label' => $this->__('Number Items'),
			'class'	=> 'validate-greater-than-zero',
		));

		$fieldset->addField('slide_speed', 'text', array(
			'name' => 'slide_speed',
			'title' => $this->__('Slide Speed'),
			'label' => $this->__('Slide Speed'),
			'class'	=> 'validate-greater-than-zero',
		));
		
		$fieldset->addField('auto_play', 'select', array(
			'name' => 'auto_play',
			'title' => $this->__('Auto-Start'),
			'label' => $this->__('Auto-Start'),
			'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
		));

		$fieldset->addField('hover_stop', 'select', array(
			'name' => 'hover_stop',
			'title' => $this->__('Stop On Hover'),
			'label' => $this->__('Stop On Hover'),
			'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
		));
		
		$fieldset->addField('navigation', 'select', array(
			'name' => 'navigation',
			'title' => $this->__('Navigation'),
			'label' => $this->__('Navigation'),
			'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
		));
		
		$fieldset->addField('pagination', 'select', array(
			'name' => 'pagination',
			'title' => $this->__('Pagination'),
			'label' => $this->__('Pagination'),
			'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray(),
		));
		
		if ($group = Mage::registry('ibanners_group')) {
			$config = $group->getSetting();			
			$form->setValues(array_merge($group->getData(), unserialize($config)));			
		}

		return parent::_prepareForm();
	}

	/**
	 * Retrieve an array of all of the stores
	 *
	 * @return array
	 */
	protected function _getStores()
	{
		$stores = Mage::getResourceModel('core/store_collection');
		$options = array(0 => $this->__('Global'));
		
		foreach($stores as $store) {
			$options[$store->getId()] = $store->getWebsite()->getName() . ': ' . $store->getName();
		}

		return $options;
	}
}
