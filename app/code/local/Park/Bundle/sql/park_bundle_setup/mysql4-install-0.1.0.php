<?php

$setup = $this;

$setup->startSetup();

$setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'price_display', array(
    'group'                         => 'Prices',
    'sort_order'                    => 6,
    'type'                          => 'varchar',
    'backend'                       => '',
    'frontend'                      => '',
    'label'                         => 'Price Display',
    'input'                         => 'text',
    'class'                         => '',
    'source'                        => '',
    'global'                        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'                       => 1,
    'required'                      => 0,
    'user_defined'                  => 1,
    'default'                       => '',
    'visible_on_front'              => 0,
    'unique'                        => 0,
    'is_configurable'               => 0,
));
$setup->updateAttribute('catalog_product', 'price_display', array('used_in_product_listing' => 1));

$setup->endSetup();