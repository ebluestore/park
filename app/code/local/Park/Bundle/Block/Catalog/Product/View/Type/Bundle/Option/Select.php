<?php

class Park_Bundle_Block_Catalog_Product_View_Type_Bundle_Option_Select
    extends Mage_Bundle_Block_Catalog_Product_View_Type_Bundle_Option_Select
{
    public function getSelectionTitlePrice($_selection, $includeContainer = true)
    {
        $this->setFormatProduct($_selection);
        $priceTitle = $this->escapeHtml($_selection->getName());        
        return $priceTitle;
    }

    public function getTourPrice($_selection)
    {
        $price = $this->getProduct()->getPriceModel()->getSelectionPreFinalPrice($this->getProduct(), $_selection, 1);
        $priceValue = $this->formatPriceString($price);
        return $priceValue;
    }
}
