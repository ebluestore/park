<?php
$this->startSetup();

$this->run("
ALTER TABLE `{$this->getTable('sales_flat_order_item')}` ADD COLUMN `reservation_time` varchar(255) NOT NULL default '';
");

$this->endSetup();

?>