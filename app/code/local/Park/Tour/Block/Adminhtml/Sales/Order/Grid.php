<?php
 
class Park_Tour_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('tour_order_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _getCollectionClass()
    {
        return 'sales/order_item_collection';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass())->addFieldToFilter('parent_item_id', array('null' => true));
        $collection->join(array('orders' => 'sales/order_grid'), 'orders.entity_id=main_table.order_id', array('increment_id'=>'increment_id','store_id'=>'store_id','billing_name'=>'billing_name','shipping_name'=>'shipping_name','base_grand_total'=>'base_grand_total','grand_total'=>'grand_total','status'=>'status','base_currency_code'=>'base_currency_code','order_currency_code'=>'order_currency_code'), null,'left');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $helper = Mage::helper('tour'); 
        $currency = Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE);
        $this->addColumn('real_order_id', array(
            'header'=> $helper->__('Reservation #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        $this->addColumn('reservation_time', array(
            'header'=> $helper->__('Reservation Date&Time'),
            'type' => 'datetime',
            'width'     => '150px',
            'renderer'  => 'Park_Tour_Block_Adminhtml_Sales_Order_Renderer_Reservation',
            'filter_condition_callback' => array($this, '_reservationFilter'),
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => $helper->__('Purchased From (Store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
            ));
        }

        $this->addColumn('name', array(
            'header' => $helper->__('Tour Name'),
            'index' => 'name',
        ));

        $this->addColumn('shipping_name', array(
            'header' => $helper->__('Lead Traveler Name'),
            'index' => 'shipping_name',
        ));

        $this->addColumn('billing_name', array(
            'header' => $helper->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('base_grand_total', array(
            'header' => $helper->__('Price'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ));

        // $this->addColumn('grand_total', array(
        //     'header' => $helper->__('G.T. (Purchased)'),
        //     'index' => 'grand_total',
        //     'type'  => 'currency',
        //     'currency' => 'order_currency_code',
        // ));

        $this->addColumn('created_at', array(
            'header' => $helper->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));
        
        $this->addColumn('status', array(
            'header' => $helper->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn('action',
                array(
                    'header'    => $helper->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getOrderId',
                    'actions'   => array(
                        array(
                            'caption' => $helper->__('View'),
                            'url'     => array('base'=>'*/sales_order/view'),
                            'field'   => 'order_id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
            ));
        }
 
        $this->addExportType('*/*/exportTourCsv', $helper->__('CSV'));
        $this->addExportType('*/*/exportTourXml', $helper->__('Excel XML'));
 
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _reservationFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $this->getCollection()->addAttributeToFilter('reservation_time', array(
            'from' => $value['orig_from'],
            'to' => $value['orig_to'],
            'date' => true,
        ));
        
        return $this;
    }
}