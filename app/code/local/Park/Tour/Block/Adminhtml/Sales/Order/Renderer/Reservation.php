<?php  

class Park_Tour_Block_Adminhtml_Sales_Order_Renderer_Reservation extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $options = $row->getData('product_options');
 		$options = unserialize($options);
 		$str_time = array();
 		foreach ($options['options'] as $opt) {
 			if($opt['label'] == 'Date' || $opt['label'] == 'Time'){
 				$str_time[] = $opt['value'];
 			}
 		}
 		return implode(" ",$str_time);
    }
}
	
?>