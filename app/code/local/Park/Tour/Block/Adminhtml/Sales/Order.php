<?php
 
class Park_Tour_Block_Adminhtml_Sales_Order extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'tour';
        $this->_controller = 'adminhtml_sales_order';
        $this->_headerText = Mage::helper('tour')->__('Tour Order');
 
        parent::__construct();
        $this->_removeButton('add');
    }
}