<?php

class Park_Tour_Model_Observer
{
    public function saveReservationTime($observer)
    {        
		$order = $observer->getEvent()->getOrder();
		$order_id = $order->getId();
		$reservation_time = Mage::app()->getFrontController()->getRequest()->getPost('reservation_time');
		foreach ($reservation_time as $time) {
			$post_data = explode("|",$time);
			$order_item = Mage::getResourceModel('sales/order_item_collection')
													->addFieldToFilter('product_id', $post_data[0])
                                                    ->addFieldToFilter('order_id', $order_id)
                                                    ->getFirstItem();
            $time_value = date('Y-m-d H:i:s',strtotime($post_data[1]));
            $order_item->setReservationTime($time_value)->save();
		}
    }
}