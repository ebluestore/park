<?php
 
class Park_Tour_Adminhtml_OrderController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Tour Order'));
        $this->loadLayout();
        $this->_setActiveMenu('sales/sales');
        $this->_addContent($this->getLayout()->createBlock('tour/adminhtml_sales_order'));
        $this->renderLayout();
    }
 
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('tour/adminhtml_sales_order_grid')->toHtml()
        );
    }
 
    public function exportTourCsvAction()
    {
        $fileName = 'tour_orders.csv';
        $grid = $this->getLayout()->createBlock('tour/adminhtml_sales_order_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
 
    public function exportTourXmlAction()
    {
        $fileName = 'tour_orders.xml';
        $grid = $this->getLayout()->createBlock('tour/adminhtml_sales_order_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}