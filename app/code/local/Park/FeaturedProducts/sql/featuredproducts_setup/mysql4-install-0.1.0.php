<?php

$setup = $this;

$setup->startSetup();

$setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'featured', array(
    'group'             			=> 'General',
    'type'              			=> 'int',
    'backend'           			=> '',
    'frontend'          			=> '',
    'label'             			=> 'Featured Product',
    'input'             			=> 'boolean',
    'class'             			=> '',
    'source'            			=> 'eav/entity_attribute_source_boolean',
    'global'            			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'              			=> 1,
	'required'             			=> 0,
	'user_defined'         			=> 1,
	'searchable'           			=> 0,
	'filterable'           			=> 0,
	'comparable'           			=> 0,
	'visible_on_front'     			=> 0,
	'visible_in_advanced_search'    => 0,
	'unique'            			=> 0,
	'default'            			=> 0
));

$setup->endSetup();