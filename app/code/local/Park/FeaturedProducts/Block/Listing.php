<?php

class Park_FeaturedProducts_Block_Listing extends Mage_Catalog_Block_Product_List 
{

	protected function _getProductCollection()
    {
        $collection = Mage::getResourceModel('catalog/product_collection');
        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $collection->addAttributeToSelect($attributes)
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToFilter(array(array('attribute' => 'featured','eq' => 1,)), null, 'left')
                ->addStoreFilter();
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
        $this->_productCollection = $collection;
        return $this->_productCollection;
    }

}