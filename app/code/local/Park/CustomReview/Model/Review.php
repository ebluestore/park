<?php

class Park_CustomReview_Model_Review extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
    	parent::_construct();
        $this->_init('customreview/review');
    }  
}