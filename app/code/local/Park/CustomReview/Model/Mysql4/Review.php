<?php

class Park_CustomReview_Model_Mysql4_Review extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {  
        $this->_init('customreview/review', 'review_id');
    }  
}