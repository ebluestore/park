<?php

class Park_CustomReview_Model_Mysql4_Review_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
    	parent::_construct();
        $this->_init('customreview/review');
    }

    public function addActiveFilter()
    {
    	return $this->addFieldToFilter('active', 1);
    }

    public function addProductFilter($product_id)
    {
    	return $this->addFieldToFilter('product_id', $product_id);
    }
}