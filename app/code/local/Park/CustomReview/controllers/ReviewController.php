<?php

class Park_CustomReview_ReviewController extends Mage_Core_Controller_Front_Action
{
    public function ratingAction()
    {
        $result = array();
        $id = $this->getRequest()->getPost('review_id');
        $review = Mage::getModel('customreview/review')->load($id);
        $review->setHelpfullCounter($review->getHelpfullCounter() + 1);
        $result['success'] = false;
        try {
            $review->save();
            $result['success'] = true;
        }
        catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            Mage::logException($e);
        }
        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
}