<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE {$this->getTable('review_custom')} (
    `review_id` int(11) NOT NULL,
    `product_id` int(11) NOT NULL,
    `author` varchar(255) NOT NULL,
    `date` varchar(100) NOT NULL,
    `title` varchar(255) NOT NULL,
    `point` tinyint(2) NOT NULL,
    `description` text NOT NULL,
    `image_1` varchar(255) NOT NULL,
    `image_2` varchar(255) NOT NULL,
    `image_3` varchar(255) NOT NULL,
    `image_4` varchar(255) NOT NULL,
    `image_5` varchar(255) NOT NULL,
    `order_id` varchar(100) NOT NULL,
    `order_date` varchar(100) NOT NULL,
    `via_facebook` smallint(1) NOT NULL DEFAULT '0',
    `fb_link` text NOT NULL,
    `comment` smallint(1) NOT NULL DEFAULT '0',
    `comment_content` text NOT NULL,
    `helpfull_counter` int(6) NOT NULL,
    `active` int(1) NOT NULL,
  	PRIMARY KEY (`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;	
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
	 