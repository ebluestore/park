<?php

class Park_CustomReview_Block_Review extends Mage_Core_Block_Template
{

	public function getReviewsCollection($product_id)
	{
		$collection = Mage::getResourceModel('customreview/review_collection')->addActiveFilter()
																			  ->addProductFilter($product_id);
		return $collection;
	}

	public function getReviewTotalPoint($product_id)
	{
		$reviews = $this->getReviewsCollection($product_id);
        $counter = 0;
        foreach ($reviews as $review) {
            $counter = $counter + $review->getPoint();
        }
        return $counter;
	}

}