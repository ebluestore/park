var centralparkFunction = function() {
}();

jQuery(document).ready(function() {  
    jQuery(".product-image-thumbs").owlCarousel({
        responsive: false,
        items: 7,
        navigation: true,
        pagination: false,
        navigationText: ["previous","next"]
    });

    jQuery("a[rel='media-gallery'],a.review-gallery").colorbox({transition:"fade"});
    
    jQuery(".review_update").click(function(){
        var ajaxurl = jQuery("#count_url").val();
        var review_id = jQuery(this).val();

        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            data: { review_id: review_id },
            complete: function(){
            },
            success: function(data) {
                var result = data.evalJSON(true);
                if(result.success == true){
                    jQuery('.helpful_review_' + review_id).text('Thank you for your vote.')
                    return false;
                } else {
                    jQuery('.helpful_review_' + review_id).text('Error! Please try again.')
                    window.location.reload(true);
                }
            }
        });
    });

    jQuery("div.review-pager").jPages({
        containerID : "review-listing",
        perPage     : 3,
        previous    : '« Previous Reviews ',
        next        : 'More Reviews »',
        callback    : function( pages, items ){
            if(jQuery('.jp-next').hasClass('jp-disabled')){
                jQuery('.jp-next').hide();
                jQuery('.jp-previous').text('« Previous Reviews');
            } else {
                jQuery('.jp-next').show();
                jQuery('.jp-previous').text('« Previous Reviews |');
            }
        }

    });

    var showChar = 260;
    var ellipsestext = "...";
    var moretext = "More &#9660;";
    var lesstext = "Less &#9650;";
    jQuery('.cr-content').each(function() {
        var content = jQuery(this).html();
        if(content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
            var tmphtml = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink less">' + lesstext + '</a></span>';            
            jQuery(this).html(tmphtml);
        }
    });

    jQuery(".morelink").click(function(){
        if(jQuery(this).hasClass("less")) {
            jQuery(this).removeClass("less");
            jQuery(this).html(moretext);
        } else {
            jQuery(this).addClass("less");
            jQuery(this).html(lesstext);
        }
        jQuery(this).parent().prev().toggle();
        jQuery(this).prev().toggle();
        return false;
    });

    jQuery(".morelink").trigger('click');
    jQuery('.product-col-right .first-time dd input.datetime-picker').click(function(){
        jQuery('.product-col-right .first-time dd img.v-middle').trigger('click');
    })

});